from flask import Flask
import configparser
import mysql.connector
from mysql.connector import errorcode

app = Flask(__name__)

@app.route('/')
def index():
    config = configparser.ConfigParser()
    config.read('config.ini')
    db_host=config['mysql']['DB_HOST']
    db_pass=config['mysql']['DB_PASS']
    db_user=config['mysql']['DB_USER']
    db_name=config['mysql']['DB_NAME']
    msg = 'Las variables de entorno son: DB_HOST=' + db_host + \
      ' - DB_NAME=' + db_name + ' - DB_USER=' + db_user + ' - DB_PASS=' + db_pass + "\n<br>"
    try:
      cnx = mysql.connector.connect(
        host=db_host,
        user=db_user,
        password=db_pass,
        database=db_name
      )
    except mysql.connector.Error as err:
      if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        msg += "usuario o clave incorrectos"
      elif err.errno == errorcode.ER_BAD_DB_ERROR:
        msg += "La base de datos no existe"
      elif err.errno == errorcode.CR_CONN_HOST_ERROR:
        msg += "No es posible conectar al host "+db_host+" por el puerto predeterminado 3306"
      else:
        msg = err
    else:
      msg += "conexión a la BD exitosa"
      cnx.close()
    return msg

app.run(host='0.0.0.0', port=80)
