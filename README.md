# Test Conexión MySQL
## Objetivo 
Probar los parámetros de conexión a base de datos MySQL desde una aplicación en python y flask
## Requisitos
Tener previamente instalado:
- debian 11 al día en sus parches
- base de datos MySQL 8.0.x (local o externa)
- descargue el código con ```git clone https://gitlab.com/c7807/test-conexion-mysql.git```
## Instalar la APP
```bash
apt install python3 python3-pip
pip install mysql-connector-python
pip install flask
```
## Configuración
Las variables de configuración se encuentran en un archivo llamado config.ini, en el cual se encuentran las variables del sistema para conectarse a la base de datos mysql, se requiere modificar el archivo para realizar la conexión a la BD requerida.
## Ejecutando la aplicación web
Para ejecutar el servidor escriba
```bash
python3 app.py
```
